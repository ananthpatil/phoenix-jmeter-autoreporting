package com.jmeter;

import java.util.List;

public class DefectList
{
   private List<DefectSetterAndGetter> dfList;

   /**
    * @return the List of defects
    */
   public List<DefectSetterAndGetter> getDefList()
   {
      return dfList;
   }

   /**
    * @param dfList set the defect list
    */
   public void setDefList(List<DefectSetterAndGetter> dfList)
   {
      this.dfList = dfList;
   }
}