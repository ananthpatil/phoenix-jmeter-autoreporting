package com.jmeter;

public class DefectSetterAndGetter
{

   private String defectString;

   private String urlDetails;

   /**
    * <p>
    * Method set the defect description
    * </p>
    * 
    * @param defectString
    */
   public void setDefectDesc(String defectString)
   {
      this.defectString = defectString;
   }

   /**
    * <p>
    * Method to get the defect string
    * </p>
    * 
    * @return defect string
    */
   public String getDefectDesc()
   {
      return defectString;
   }

   /**
    * The method to set the defect URL
    * 
    * @param urlDetails
    */
   public void setDefectUrl(String urlDetails)
   {
      this.urlDetails = urlDetails;
   }

   /**
    * Method to get the defect URL
    * 
    * @return
    */
   public String getDefectUrl()
   {
      return urlDetails;
   }
}
