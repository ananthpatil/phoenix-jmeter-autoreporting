package com.jmeter;

import java.io.*;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

public class JmeterReport_v1
{

   protected String outputFolderPath;

   private String tokenSeparator = "^^";

   protected BrandMapping brandObject;

   private Map<String, String> brandMap;

   /**
    * <p>
    * Constructor to initialise the brand map and result folder path
    * </p>
    * 
    * @param brandMap
    * @param outputFolderPath
    */
   JmeterReport_v1(Map<String, String> brandMap, String outputFolderPath)
   {
      this.brandMap = brandMap;
      this.outputFolderPath = outputFolderPath;
      setFileName();
   }

   private void setFileName()
   {
      if (!outputFolderPath.contains("\\") && !outputFolderPath.contains("/"))
         outputFolderPath = System.getProperty("user.dir") + File.separator + "DefectSummary";
   }

   /**
    * <p>
    * The method to matches multiple string
    * </p>
    * 
    * @return
    */
   @SuppressWarnings("unused")
   private Map<String, String> multipleMatches()
   {
      Map<String, String> stringMap = new HashMap<String, String>();
      try
      {
         stringMap.put("bookingref", "bookingref");
         stringMap.put("bookingreference", "bookingreference");
         stringMap.put("testcaseid", "testcaseid");
         stringMap.put("testcasedescription", "testcasedescription");
         stringMap.put("testdata", "testdata");
      }
      catch (Exception e)
      {
         System.out.println("Exception in multipleMatches method");
      }
      return stringMap;
   }

   /**
    * The method to create element
    * 
    * @param doc
    * @param ele
    * @param tagName
    * @return
    * @author Anant Patil
    */
   private Element createElement(Document doc, Element ele, String tagName)
   {
      Element hashTree = null;
      try
      {
         hashTree = doc.createElement(tagName);
         ele.appendChild(hashTree);
         // System.out.println("Element was created:"+tagName);
      }
      catch (Exception e)
      {
         System.out.println("Element didn't created:" + e.getMessage());
      }
      return hashTree;
   }

   /**
    * The method to get the three integer digit in String format
    * 
    * @return String
    * @param
    * @author Anant Patil
    */
   private String getIntegerVal()
   {
      Random rand = new Random();
      Integer tot = 1;
      for (int j = 1; j <= 3; j++)
      {
         if (j == 1)
            tot = tot * j;
         else
         {
            tot = tot * 10;
            tot = tot + rand.nextInt(9) + 1;
         }
      }
      return tot.toString();
   }

   /**
    * The following method used To create the XMl file
    * 
    * @return void
    * @author Anant patil
    */
   private void createXmlFile()
   {
      try
      {
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         Document doc = builder.newDocument();

         Element root = doc.createElement("SummaryResult");
         doc.appendChild(root);

         TransformerFactory tFatcory = TransformerFactory.newInstance();
         Transformer trans = tFatcory.newTransformer();
         DOMSource src = new DOMSource(doc);

         /*
          * String xmlFilName = System.getProperty("user.dir") + File.separator + getFileName();
          */
         String xmlFilName = outputFolderPath.replaceAll("\\.\\w+", "") + ".xml";

         File file = new File(xmlFilName);
         if (file.exists())
            file.delete();
         StreamResult result = new StreamResult(new File(xmlFilName));
         trans.transform(src, result);
         System.out.println("New XML File created :" + xmlFilName + " !");
      }
      catch (ParserConfigurationException e1)
      {
         System.out.println("The parseConfiguration exception:" + e1);
      }
      catch (TransformerConfigurationException e2)
      {
         System.out.println("The Transformer Configuration error:" + e2);
      }
      catch (TransformerException e3)
      {
         System.out.println("TransfomerException:" + e3);
      }
   }

   /**
    * <p>
    * The following method is used to add the defect details to XML
    * </p>
    * 
    * @returns void
    * @author Anant
    */
   private void addDetails(String userName, String currentDate, String machineIP,
      String envDetails, BrandMapping brandMapObject)
   {
      String summryDesc = " mismatch is happening from the ", xmlFileName;
      try
      {
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         xmlFileName = outputFolderPath.replaceAll("\\.\\w+", "") + ".xml";
         File fin = new File(xmlFileName);
         if (fin.exists())
         {
            Document doc = builder.parse(fin);
            Element root = doc.getDocumentElement();

            Element generalInfo = createElement(doc, root, "GeneralInfo");

            Element user = createElement(doc, generalInfo, "UserName");
            user.setTextContent(userName);

            Element dateExecute = createElement(doc, generalInfo, "DateOfExecution");
            dateExecute.setTextContent(currentDate);

            Element ip = createElement(doc, generalInfo, "MachineIP");
            ip.setTextContent(machineIP);

            Element env = createElement(doc, generalInfo, "Environment");
            env.setTextContent(envDetails);

            for (Map.Entry<String, Scenario> brandmap : brandMapObject.getBrands().entrySet())
            {
               String brandsName = brandmap.getKey().trim();
               Scenario sceObj = brandmap.getValue();
               Element brands = createElement(doc, generalInfo, "Brands");
               Element brandname = createElement(doc, brands, "Brandname");
               brandname.setTextContent(brandsName);
               Map<String, ArrayList<TestCase>> scenarioStoredMap = sceObj.getSecnario();
               for (Map.Entry<String, ArrayList<TestCase>> scenarioMap : scenarioStoredMap
                  .entrySet())
               {
                  String scenarioName = scenarioMap.getKey().replace(".csv", "");
                  ArrayList<TestCase> tstList = scenarioMap.getValue();
                  for (int p = 0; p < tstList.size(); p++)
                  {
                     TestCase tstCaseObj = tstList.get(p);
                     LinkedHashMap<String, DefectList> tstDefectMap = tstCaseObj.getTestCase();
                     for (Map.Entry<String, DefectList> tstCaseDefectMap : tstDefectMap.entrySet())
                     {

                        String testCaseId;

                        testCaseId = tstCaseDefectMap.getKey().toLowerCase().trim();
                        DefectList defectList = tstCaseDefectMap.getValue();
                        List<DefectSetterAndGetter> defNameAndUrl = defectList.getDefList();

                        for (int t = 0; t < defNameAndUrl.size(); t++)
                        {
                           String stepsTpRep = "", compName = null, pageName1 = null, pageName2 =
                              null, expected = null, actual = null, defectName, urlToLog, mainUrl =
                              null;
                           String bookref, testDataVal = "", insertWord = " to ";
                           DefectSetterAndGetter df = defNameAndUrl.get(t);
                           defectName = df.getDefectDesc().toLowerCase().trim();
                           urlToLog = df.getDefectUrl().trim();
                           boolean reportFlag = true;

                           if (!defectName.contains("=") && urlToLog.contains(tokenSeparator))
                           {
                              try
                              {
                                 String[] infoArray = urlToLog.split("\\^\\^");
                                 compName = defectName.toLowerCase().trim();
                                 String[] str = null;
                                 try
                                 {
                                    if (compName.contains("|"))
                                    {
                                       str = compName.split("\\|");
                                       compName = str[0].replaceAll("\\w*\\_", "");
                                       pageName1 =
                                          str[0].substring(0, str[0].lastIndexOf("_")).replaceAll(
                                             "\\w*\\_", "");
                                       pageName2 =
                                          str[1].substring(0, str[1].lastIndexOf("_")).replaceAll(
                                             "\\w*\\_", "");
                                    }
                                    else
                                    {
                                       pageName1 = defectName.substring(0, defectName.indexOf("_"));
                                       pageName2 = "";
                                       insertWord = " ";
                                    }
                                 }
                                 catch (Exception e)
                                 {
                                 }
                                 expected = infoArray[0].toLowerCase().trim();
                                 actual = infoArray[1].toLowerCase().trim();
                                 mainUrl = infoArray[2].trim();
                                 bookref = infoArray[3].trim();
                                 testDataVal = infoArray[4].trim();
                                 if (p == 0)
                                    summryDesc = " component verification failed from ";
                                 else
                                    summryDesc = " component verification passed from ";
                                 stepsTpRep =
                                    stepsTpRep + "1.Navigate to the " + pageName1
                                       + " page check the " + compName + " value\n";
                                 stepsTpRep =
                                    stepsTpRep + "2.Navigate to the " + pageName2
                                       + " page check the " + compName + " value\n";
                                 stepsTpRep = stepsTpRep + "3.Booking ref:" + bookref;

                              }
                              catch (Exception e)
                              {
                                 reportFlag = false;
                              }
                           }
                           else
                           {
                              try
                              {
                                 String[] infoArray = urlToLog.split("\\^\\^");
                                 mainUrl = infoArray[0];
                                 compName =
                                    defectName.substring(0, defectName.indexOf(" in ")).trim()
                                       .replaceAll("\\w*\\_", "");
                                 pageName2 =
                                    defectName.substring(defectName.lastIndexOf(" in ") + 3,
                                       defectName.lastIndexOf("=")).trim();
                                 pageName1 =
                                    defectName.substring(defectName.indexOf(" in ") + 3,
                                       defectName.indexOf("=")).trim();
                                 expected =
                                    defectName.substring(defectName.indexOf("=") + 1,
                                       defectName.indexOf("|"));
                                 actual = defectName.substring(defectName.lastIndexOf("=") + 1);
                                 actual = actual.replaceAll("\\*+", "");
                                 expected = expected.replaceAll("\\*+", "");
                                 bookref = infoArray[1].trim();
                                 testDataVal = infoArray[2].trim();
                                 summryDesc = " mismatch is happening from the ";
                                 stepsTpRep =
                                    stepsTpRep + "1.Navigate to the " + pageName1
                                       + " page check the " + compName + " value\n"
                                       + "2.Navigate to the " + pageName2 + " page check the "
                                       + compName + " value\n";
                                 stepsTpRep = stepsTpRep + "3.Booking ref:" + bookref;

                              }
                              catch (Exception e)
                              {
                                 reportFlag = false;
                              }
                           }
                           if (reportFlag)
                           {

                              String tagName;
                              if (p == 0)
                                 tagName = "defectSummary";
                              else
                                 tagName = "PassSummary";

                              Element defectSummary = createElement(doc, brands, tagName);

                              Element secn = createElement(doc, defectSummary, "ScenarioName");
                              secn.setTextContent(scenarioName);

                              Element tstId = createElement(doc, defectSummary, "TestCaseID");
                              tstId.setTextContent(testCaseId);

                              Element summary = createElement(doc, defectSummary, "Summary");
                              summary.setTextContent(compName + summryDesc + pageName1 + insertWord
                                 + pageName2 + " page");

                              Element urlVal = createElement(doc, defectSummary, "UrlVal");
                              String[] urlArr = mainUrl.split("\n");
                              for (int u = 0; u < urlArr.length; u++)
                              {
                                 Element url = createElement(doc, urlVal, "URL");
                                 Element value = createElement(doc, url, "Value");
                                 value.setTextContent(urlArr[u]);
                              }

                              if (p == 0)
                              {
                                 Element StepsToReproduce =
                                    createElement(doc, defectSummary, "StepsToReproduce");
                                 String[] stepsArr = stepsTpRep.split("\n");
                                 for (int i = 0; i < stepsArr.length; i++)
                                 {
                                    Element reproduce =
                                       createElement(doc, StepsToReproduce, "Reproduce");
                                    Element steps = createElement(doc, reproduce, "Steps");
                                    steps.setTextContent(stepsArr[i]);
                                 }
                              }

                              Element expectedTag = createElement(doc, defectSummary, "Expected");
                              expectedTag.setTextContent(expected);

                              Element actualTag = createElement(doc, defectSummary, "Actual");
                              actualTag.setTextContent(actual);

                              Element testDataTag = createElement(doc, defectSummary, "TestData");
                              testDataTag.setTextContent(testDataVal);
                           }
                        }
                     }
                  }
               }
               addTestCaseDetails(scenarioStoredMap, doc, brands);
            }

            TransformerFactory tfactory = TransformerFactory.newInstance();
            Transformer trans = tfactory.newTransformer();
            DOMSource src = new DOMSource(doc);
            StreamResult result = new StreamResult(fin);
            trans.transform(src, result);
         }
      }
      catch (ParserConfigurationException e)
      {
         System.out.println("ParseConfiguration Error:" + e);
      }
      catch (SAXException e1)
      {
         System.out.println("SAX error:" + e1);
      }
      catch (IOException e2)
      {
         System.out.println("The I/O error:" + e2);
      }
      catch (TransformerConfigurationException e)
      {
         e.printStackTrace();
      }
      catch (TransformerException e3)
      {
         System.out.println("Transformer Exception:" + e3);
      }
   }

   /**
    * <p>
    * Method to create the test Element
    * </p>
    * 
    * @param doc
    * @param root
    * @param statusText
    * @param testId
    * @param scenarioName
    */
   private void createTestElement(Document doc, Element root, String statusText, String testId,
      String scenarioName)
   {
      Element tstCase = createElement(doc, root, "TestCase");

      Element status = createElement(doc, tstCase, "Status");
      status.setTextContent(statusText);

      Element tstID = createElement(doc, tstCase, "TestCaseId");
      tstID.setTextContent(testId);

      Element scenario = createElement(doc, tstCase, "ScenarioName");
      scenario.setTextContent(scenarioName);
   }

   /**
    * <p>
    * The method to add the test case details.
    * </p>
    * 
    * @param scenarioDefectMap
    * @param doc
    * @param root
    * @author Anant
    */
   private void addTestCaseDetails(Map<String, ArrayList<TestCase>> scenarioStored, Document doc,
      Element root)
   {
      float passed = 0, failed = 0, total, perPass, perFail = 0;
      String percentPass, percentFail;
      for (Map.Entry<String, ArrayList<TestCase>> scenarioMap : scenarioStored.entrySet())
      {
         try
         {
            String scenarioName = scenarioMap.getKey().replace(".csv", "");
            ArrayList<TestCase> testCaseList = scenarioMap.getValue();

            TestCase failTstObj = testCaseList.get(0);
            TestCase passTstObj = testCaseList.get(1);

            LinkedHashMap<String, DefectList> passMap = passTstObj.getTestCase();
            LinkedHashMap<String, DefectList> failMap = failTstObj.getTestCase();

            Iterator<String> passIterator = passMap.keySet().iterator();
            while (passIterator.hasNext())
            {
               String testCaseID = passIterator.next().toLowerCase().trim();
               createTestElement(doc, root, "PASS", testCaseID, scenarioName);
               passed++;
            }

            Iterator<String> failIterator = failMap.keySet().iterator();
            while (failIterator.hasNext())
            {
               String val = failIterator.next().toLowerCase().trim();
               createTestElement(doc, root, "FAIL", val, scenarioName);
               failed++;
            }
         }
         catch (Exception e)
         {
            System.out.println("Exception while adding the Test case "
               + "detail to existing xml file:" + e);
         }
      }
      total = passed + failed;
      perPass = (passed) / (total) * 100;
      perFail = (failed) / (total) * 100;
      DecimalFormat fmt = new DecimalFormat("#0.##");
      percentPass = fmt.format(perPass);
      percentFail = fmt.format(perFail);

      try
      {
         Element tstSummary = createElement(doc, root, "TestSummary");

         Element totalTag = createElement(doc, tstSummary, "Total");
         totalTag.appendChild(doc.createTextNode(String.valueOf(total)));

         Element passTag = createElement(doc, tstSummary, "Passed");
         passTag.appendChild(doc.createTextNode(String.valueOf(passed)));

         Element failTag = createElement(doc, tstSummary, "Failed");
         failTag.appendChild(doc.createTextNode(String.valueOf(failed)));

         Element perPassTag = createElement(doc, tstSummary, "PercentagePassed");
         perPassTag.appendChild(doc.createTextNode(String.valueOf(percentPass)));

         Element perFailTag = createElement(doc, tstSummary, "PercentageFailed");
         perFailTag.appendChild(doc.createTextNode(String.valueOf(percentFail)));

      }
      catch (Exception e)
      {
         System.out.println("Exception while adding the test summary to xml:" + e);
      }
   }

   /**
    * <p>
    * The method is used to convert the XML to HTML
    * </p>
    * 
    * @param xslFile
    * @return void
    * @author Anant
    */
   private void transformXmlToHtml(String xslFile)
   {
      try
      {
         System.out.println("the file:" + xslFile);
         String path = outputFolderPath;

         File dltHtmlFile = new File(path.replaceAll("\\.\\w+", "") + ".html");
         if (dltHtmlFile.exists())
         {
            if (dltHtmlFile.getName().contains(".html"))
            {
               dltHtmlFile.delete();
            }
         }
         TransformerFactory tfactory = TransformerFactory.newInstance();
         StreamSource xmlfile = new StreamSource(path.replaceAll("\\.\\w+", "") + ".xml");
         StreamSource xslfile = new StreamSource(xslFile);
         FileOutputStream htmlfile = new FileOutputStream(path.replaceAll("\\.\\w+", "") + ".html");
         Transformer trans = tfactory.newTransformer(xslfile);
         trans.transform(xmlfile, new StreamResult(htmlfile));
         System.out.println("file  converted");

      }
      catch (TransformerConfigurationException e1)
      {
         System.out.println("The TransformerConfigurationException:" + e1);
      }
      catch (FileNotFoundException e2)
      {
         System.out.println("The FileNotFoundException:" + e2);
      }
      catch (TransformerException e3)
      {
         System.out.println("The TransformerException:" + e3);
      }
   }

   /**
    * The method used to form the defect string
    * 
    * @param brandObj
    * @return String
    * @author Anant
    */
   private String defectFormation(BrandMapping brandObj)
   {
      String scenarioName, defectName, urlToLog, pageName1, pageName2, compName, actual, expected, url, bookref;
      String summryDesc = " mismatch is happening from the ", testCaseId;
      String summaryCompDesc = " component verification failed from ";

      StringBuilder builder = new StringBuilder();
      Map<String, Scenario> brands = brandObj.getBrands();

      for (Map.Entry<String, Scenario> brandsSet : brands.entrySet())
      {
         Scenario sceObejct = brandsSet.getValue();
         String brandName = brandsSet.getKey().toUpperCase();
         builder.append("====================== " + brandName
            + " SCENARIO STARTS HERE=======================\n");
         for (Map.Entry<String, ArrayList<TestCase>> scenarioMap : sceObejct.getSecnario()
            .entrySet())
         {

            scenarioName = scenarioMap.getKey().replace(".csv", "");
            ArrayList<TestCase> failList = scenarioMap.getValue();
            TestCase tstCaseObj = failList.get(0);
            LinkedHashMap<String, DefectList> tstDefectMap = tstCaseObj.getTestCase();

            for (Map.Entry<String, DefectList> tstCaseDefectMap : tstDefectMap.entrySet())
            {

               pageName1 = null;
               pageName2 = null;
               compName = "";
               bookref = "";
               testCaseId = tstCaseDefectMap.getKey().trim();
               DefectList defectList = tstCaseDefectMap.getValue();
               List<DefectSetterAndGetter> defNameAndUrl = defectList.getDefList();

               for (int t = 0; t < defNameAndUrl.size(); t++)
               {
                  DefectSetterAndGetter df = defNameAndUrl.get(t);
                  defectName = df.getDefectDesc();
                  urlToLog = df.getDefectUrl();
                  if (!defectName.contains("=") && urlToLog.contains(tokenSeparator))
                  {

                     try
                     {
                        String[] infoArray = urlToLog.split("\\^\\^");
                        compName = defectName.toLowerCase().trim();
                        String[] str = null;
                        try
                        {
                           if (compName.contains("|"))
                           {
                              str = compName.split("\\|");
                              compName = str[0].replaceAll("\\w*\\_", "");
                              pageName1 =
                                 str[0].substring(0, str[0].lastIndexOf("_")).replaceAll("\\w*\\_",
                                    "");
                              pageName2 =
                                 str[1].substring(0, str[1].lastIndexOf("_")).replaceAll("\\w*\\_",
                                    "");
                           }
                           else
                           {
                              pageName1 = defectName.substring(0, defectName.indexOf("_"));
                              pageName2 = "";
                           }
                        }
                        catch (Exception e)
                        {
                        }

                        expected = infoArray[0].toLowerCase().trim();
                        actual = infoArray[1].toLowerCase().trim();
                        url = infoArray[2].trim();
                        bookref = infoArray[3].trim();

                        builder.append("SenarioName:" + scenarioName + "\n");
                        builder.append("Summary:" + compName + summaryCompDesc + pageName1
                           + " page\n");
                        builder.append("Steps to Reproduce:-" + "\n");
                        builder.append("1.Launch the below URL" + "\n" + url + "\n");
                        builder.append("2.Navigate to the " + pageName1 + " page check the "
                           + compName + " value\n");
                        builder.append("3.Navigate to the " + pageName2 + " page check the "
                           + compName + " value\n");
                        builder.append("Expected Result:" + expected + "\n");
                        builder.append("Actual Result:" + actual + "\n");
                        builder.append("BookingRef:" + bookref + "\n");
                        builder.append("TestCasedId:" + testCaseId + "\n");
                        builder.append("\r\n");
                     }
                     catch (Exception e)
                     {
                        System.out.println("Exception in defectFormation method:" + e);
                     }
                  }
                  else
                  {
                     try
                     {
                        String[] infoArray = urlToLog.split("\\^\\^");
                        compName =
                           defectName.substring(0, defectName.indexOf(" in ")).trim()
                              .replaceAll("\\w*\\_", "");
                        pageName2 =
                           defectName.substring(defectName.lastIndexOf(" in ") + 3,
                              defectName.lastIndexOf("=")).trim();
                        pageName1 =
                           defectName.substring(defectName.indexOf(" in ") + 3,
                              defectName.indexOf("=")).trim();
                        expected =
                           defectName.substring(defectName.indexOf("=") + 1,
                              defectName.indexOf("|"));
                        actual = defectName.substring(defectName.lastIndexOf("=") + 1);
                        actual = actual.replaceAll("\\*+", "");
                        expected = expected.replaceAll("\\*+", "");

                        builder.append("SenarioName:" + scenarioName + "\n");
                        builder.append("Summary:" + compName + summryDesc + pageName1 + " to "
                           + pageName2 + " page\n");
                        builder.append("Steps to Reproduce:-" + "\n");
                        builder
                           .append("1.Launch the below URL" + "\n" + infoArray[0].trim() + "\n");
                        builder.append("2.Navigate to the " + pageName1 + " page check the "
                           + compName + " value\n");
                        builder.append("3.Navigate to the " + pageName2 + " page check the "
                           + compName + " value\n");
                        builder.append("Expected Result:" + expected + "\n");
                        builder.append("Actual Result:" + actual + "\n");
                        builder.append("BookingRef:" + infoArray[1] + "\n");
                        builder.append("TestCasedId:" + testCaseId + "\n");
                        builder.append("\r\n");
                     }
                     catch (Exception e)
                     {
                        System.out.println("Exception in defectFormation method:" + e);
                     }
                  }
               }
            }
            builder.append("================== " + brandName
               + " SCENARIO ENDS HERE======================\n");
         }
      }
      return builder.toString();
   }

   /**
    * <p>
    * The method to write the defect string to Text file
    * </p>
    * 
    * @param fileName
    * @param defectString
    * @author ANant
    */
   private void writeToTextFIle(String defectString)
   {
      BufferedWriter writer = null;
      try
      {
         String fileName = outputFolderPath.replaceAll("\\.\\w+", "") + ".txt";
         writer = new BufferedWriter(new FileWriter(fileName));
         writer.write(defectString);
         writer.close();
      }
      catch (Exception e)
      {
         System.out.println("Error occured while writting into text file:" + e);
      }
      finally
      {
         try
         {
            if (writer != null)
               writer.close();
         }
         catch (Exception e)
         {
         }
      }
   }

   /**
    * <p>
    * the method to search the scenarios in existing map
    * </p>
    * 
    * @param scenarioName
    * @param storedScenarioMap
    * @param latestFailMap
    * @param index
    * @return TestCase object
    */
   private TestCase searchScenariosInExistingMap(String scenarioName,
      Map<String, ArrayList<TestCase>> storedScenarioMap,
      LinkedHashMap<String, DefectList> latestFailMap, int index)
   {

      boolean flag1 = false, flag2;
      TestCase tstObj = new TestCase();

      for (Map.Entry<String, ArrayList<TestCase>> entrySet : storedScenarioMap.entrySet())
      {
         String storedScenarioName = entrySet.getKey();
         if (storedScenarioName.equalsIgnoreCase(scenarioName))
         {
            flag1 = true;
            ArrayList<TestCase> storedTstCaseList = entrySet.getValue();
            TestCase strdTstFailObj = storedTstCaseList.get(index);
            LinkedHashMap<String, DefectList> strdFailmap = strdTstFailObj.getTestCase();
            for (Map.Entry<String, DefectList> entryset1 : latestFailMap.entrySet())
            {
               String tstCase1 = entryset1.getKey();
               if (strdFailmap.containsKey(tstCase1))
               {

                  List<DefectSetterAndGetter> list1 = entryset1.getValue().getDefList();
                  List<DefectSetterAndGetter> list2 = strdFailmap.get(tstCase1).getDefList();
                  for (int l = 0; l < list1.size(); l++)
                  {
                     String disc1 = list1.get(l).getDefectDesc();
                     flag2 = false;
                     for (int l1 = 0; l1 < list2.size(); l1++)
                     {
                        String disc2 = list2.get(l1).getDefectDesc();
                        try
                        {
                           if (disc1.contains("="))
                           {
                              if (disc2.contains(disc1.substring(0, disc1.indexOf("="))))
                              {
                                 flag2 = true;
                                 break;
                              }
                           }
                           else
                           {
                              if (disc2.contains(disc1.trim()))
                              {
                                 flag2 = true;
                                 break;
                              }
                           }
                        }
                        catch (Exception e)
                        {
                        }
                     }
                     if (flag2 == false)
                        list2.add(list1.get(l));
                  }
                  DefectList defList = new DefectList();
                  defList.setDefList(list2);
                  strdFailmap.put(tstCase1, defList);
               }
               else
               {
                  strdFailmap.put(tstCase1, entryset1.getValue());
               }
            }
            tstObj.setTestCase(strdFailmap);
            break;
         }
      }
      if (flag1 == false)
         tstObj.setTestCase(latestFailMap);

      return tstObj;
   }

   /**
    * <p>
    * The method used to search the New defect details In map
    * </p>
    * 
    * @param testCaseIdMap
    * @param testCaseId
    * @param csvData
    * @return LinkedHashMap
    * @author Anant
    */
   private LinkedHashMap<String, DefectList> searchTextInMapForComp(
      LinkedHashMap<String, DefectList> testCaseIdMap, String testCaseId, String[] csvData,
      String bookRef, String testDataVal)
   {

      LinkedHashMap<String, DefectList> testCaseIdMap1 = new LinkedHashMap<String, DefectList>();
      boolean flag1 = false;
      try
      {
         csvData[3] = csvData[3].replace("[", "").replace("]", "");
         for (Entry<String, DefectList> tst : testCaseIdMap.entrySet())
         {
            String tstKey = tst.getKey();
            if (tstKey.equalsIgnoreCase(testCaseId))
            {
               DefectList dfList = tst.getValue();
               List<DefectSetterAndGetter> ltd = dfList.getDefList();
               boolean flag2 = false;
               flag1 = true;
               for (int j = 0; j < ltd.size(); j++)
               {
                  DefectSetterAndGetter d = ltd.get(j);
                  String description = d.getDefectDesc();
                  if (description.contains(csvData[4].trim()))
                  {
                     flag2 = true;
                     break;
                  }
               }

               if (flag2 == false)
               {

                  DefectList list = new DefectList();
                  List<DefectSetterAndGetter> defList1 = new ArrayList<DefectSetterAndGetter>();
                  DefectSetterAndGetter defMap = new DefectSetterAndGetter();
                  defMap.setDefectDesc(csvData[4].trim());
                  defMap.setDefectUrl(csvData[2] + tokenSeparator + csvData[1] + tokenSeparator
                     + csvData[3] + tokenSeparator + bookRef + tokenSeparator + testDataVal);
                  defList1.add(defMap);
                  ltd.addAll(defList1);
                  list.setDefList(ltd);
                  testCaseIdMap1.put(tstKey, list);
               }
               break;
            }

         }
         if (flag1 == false)
         {
            DefectList list = new DefectList();
            List<DefectSetterAndGetter> defList = new ArrayList<DefectSetterAndGetter>();
            DefectSetterAndGetter defMap = new DefectSetterAndGetter();
            defMap.setDefectDesc(csvData[4].trim());
            defMap.setDefectUrl(csvData[2] + tokenSeparator + csvData[1] + tokenSeparator
               + csvData[3] + tokenSeparator + bookRef + tokenSeparator + testDataVal);
            defList.add(defMap);
            list.setDefList(defList);
            testCaseIdMap1.put(testCaseId, list);
         }
      }
      catch (Exception e)
      {
         System.out.println("Exception occured in searchTextInMapForComp:" + e);
      }
      return testCaseIdMap1;
   }

   /**
    * <p>
    * The method used to search the New defect details In map
    * </p>
    * 
    * @param testCaseIdMap
    * @param testCaseIdMap
    * @param csvString
    * @param urlVal
    * @return LinkedHashMap
    * @return Anant
    */
   private LinkedHashMap<String, DefectList> searchTextInMap(
      LinkedHashMap<String, DefectList> testCaseIdMap, String testCaseId, String csvString,
      String urlVal, String bookref, String testDataVal)
   {

      LinkedHashMap<String, DefectList> testCaseIdMap1 = new LinkedHashMap<String, DefectList>();
      boolean flag1 = false;
      try
      {
         urlVal = urlVal.replace("[", "").replace("]", "");
         for (Map.Entry<String, DefectList> tst : testCaseIdMap.entrySet())
         {
            String tstKey = tst.getKey();
            if (tstKey.equalsIgnoreCase(testCaseId))
            {
               DefectList dfList = tst.getValue();
               List<DefectSetterAndGetter> ltd = dfList.getDefList();
               boolean flag2 = false;
               flag1 = true;
               for (int index = 0; index < ltd.size(); index++)
               {
                  DefectSetterAndGetter d = ltd.get(index);
                  String description = d.getDefectDesc();
                  try
                  {
                     if (description.contains(csvString.substring(0, csvString.indexOf("="))))
                     {
                        flag2 = true;
                        break;
                     }
                  }
                  catch (Exception e)
                  {
                  }
               }

               if (flag2 == false)
               {
                  DefectList list = new DefectList();
                  DefectSetterAndGetter defMap = new DefectSetterAndGetter();
                  List<DefectSetterAndGetter> defList1 = new ArrayList<DefectSetterAndGetter>();

                  defMap.setDefectDesc(csvString);
                  defMap.setDefectUrl(urlVal + tokenSeparator + bookref + tokenSeparator
                     + testDataVal);
                  defList1.add(defMap);
                  ltd.addAll(defList1);
                  list.setDefList(ltd);
                  testCaseIdMap1.put(tstKey, list);
               }
               break;
            }
         }
         if (flag1 == false)
         {
            DefectList list = new DefectList();
            DefectSetterAndGetter defMap = new DefectSetterAndGetter();
            List<DefectSetterAndGetter> defList = new ArrayList<DefectSetterAndGetter>();
            defMap.setDefectDesc(csvString);
            defMap.setDefectUrl(urlVal + tokenSeparator + bookref + tokenSeparator + testDataVal);
            defList.add(defMap);
            list.setDefList(defList);
            testCaseIdMap1.put(testCaseId, list);
         }
      }
      catch (Exception e)
      {
         System.out.println("Exception occured in searchTextInMap:" + e);
      }
      return testCaseIdMap1;
   }

   /**
    * <p>
    * The method used extract the unique defect failures from CSV file
    * </p>
    * 
    * @param csvFile
    * @return
    */
   private Scenario processCSVFiles(List<File> csvFile)
   {

      Scenario scenarioObject = new Scenario();
      Map<String, ArrayList<TestCase>> scenarioMap;
      scenarioMap = new LinkedHashMap<String, ArrayList<TestCase>>();

      CSVReader reader = null;

      for (int c = 0; c < csvFile.size(); c++)
      {
         try
         {
            ArrayList<TestCase> tstCaseList = new ArrayList<TestCase>();
            TestCase tstCaseObj = new TestCase();
            TestCase tstCasePassObj = new TestCase();

            String scenarioName, urlVal, headerString, bookRef = null, testCaseId = null;
            String testCaseIdDummy = null, testDescription = null, testDataVal = null;
            String[] csvData, headerNames;
            int bookIndex = -1, testcaseIdIndex = -1, testDescIndex = -1, testDataIndex = -1;

            LinkedHashMap<String, DefectList> testCaseIdFailMap =
               new LinkedHashMap<String, DefectList>();
            LinkedHashMap<String, DefectList> testCaseIdPassMap =
               new LinkedHashMap<String, DefectList>();
            List<DefectSetterAndGetter> defFailedList, defPassedList;

            scenarioName = csvFile.get(c).getName().replaceAll("\\d+", "");
            reader = new CSVReader(new FileReader(csvFile.get(c)));
            headerNames = reader.readNext();
            headerString = Arrays.toString(headerNames).toLowerCase().replace(" ", "");
            headerString = headerString.replace("[", "").replace("]", "");

            for (int h = 0; h < headerNames.length; h++)
            {
               String headerName = headerNames[h].trim().toLowerCase();
               headerName = headerName.replace(" ", "");

               if (headerName.contains("bookingref") || headerName.contains("bookref"))
                  bookIndex = h;
               else
                  bookRef = null;

               if (headerName.contains("testcaseid"))
                  testcaseIdIndex = h;
               else
                  testCaseId = "tsr" + getIntegerVal();

               if (headerName.contains("testcasedescription"))
                  testDescIndex = h;
               else
                  testDescription = "Test description was missed";

               if (headerName.contains("testdata"))
                  testDataIndex = h;
               else
                  testDataVal = "Test data was missed";
            }

            defFailedList = new ArrayList<DefectSetterAndGetter>();
            defPassedList = new ArrayList<DefectSetterAndGetter>();

            DefectList list = new DefectList();
            DefectList passList = new DefectList();
            int incr = 0, incr1 = 0;
            boolean enter = false;
            while ((csvData = reader.readNext()) != null)
            {
               enter = true;
               try
               {
                  String status = csvData[0].trim();
                  if (bookIndex >= 0)
                     bookRef = csvData[bookIndex];

                  if (testcaseIdIndex >= 0)
                  {
                     testCaseId = csvData[testcaseIdIndex].toLowerCase();
                     if (testCaseId.equals("null") || testCaseId.length() == 0)
                     {
                        if (incr == 0)
                        {
                           testCaseId = "tcr" + getIntegerVal().toLowerCase();
                           testCaseIdDummy = testCaseId.toLowerCase();
                        }
                        else
                           testCaseId = testCaseIdDummy.toLowerCase();
                     }
                  }

                  if (testDescIndex >= 0)
                  {
                     testDescription = csvData[testDescIndex];
                     if (testDescription.length() == 0 || testDescription.equals("null"))
                        testDescription = "Test description was missed";
                  }

                  if (testDataIndex >= 0)
                  {
                     testDataVal = csvData[testDataIndex];
                     if (testDataVal.length() == 0 || testDataVal.equals("null"))
                        testDataVal = "Test data was missed";
                  }
                  if (headerString
                     .contains("status,actualresult,expectedresult,url,component,filename"))
                  {
                     try
                     {
                        bookRef = bookRef.replace("[]", "");
                        DefectSetterAndGetter defMap = new DefectSetterAndGetter();
                        defMap.setDefectDesc(csvData[4].trim().toLowerCase());
                        defMap.setDefectUrl(csvData[2] + tokenSeparator + csvData[1]
                           + tokenSeparator + csvData[3].replace("[", "").replace("]", "")
                           + tokenSeparator + bookRef + tokenSeparator + testDataVal);
                        if (status.equalsIgnoreCase("failed"))
                        {
                           if (incr == 0)
                           {
                              defFailedList.add(defMap);
                              list.setDefList(defFailedList);
                              testCaseIdFailMap.put(testCaseId, list);
                           }
                           else
                           {
                              LinkedHashMap<String, DefectList> testCaseIdMap1 =
                                 searchTextInMapForComp(testCaseIdFailMap, testCaseId, csvData,
                                    bookRef, testDataVal);
                              testCaseIdFailMap.putAll(testCaseIdMap1);
                           }
                           incr++;
                        }

                        if (status.equalsIgnoreCase("passed"))
                        {
                           if (incr1 == 0)
                           {
                              defPassedList.add(defMap);
                              passList.setDefList(defPassedList);
                              testCaseIdPassMap.put(testCaseId, passList);
                           }
                           else
                           {
                              LinkedHashMap<String, DefectList> testCaseIdMap1 =
                                 searchTextInMapForComp(testCaseIdPassMap, testCaseId, csvData,
                                    bookRef, testDataVal);
                              testCaseIdPassMap.putAll(testCaseIdMap1);
                           }
                           incr1++;
                        }
                     }
                     catch (Exception e)
                     {
                     }
                  }
                  else
                  {
                     try
                     {
                        urlVal = "";
                        boolean compare = true;
                        for (int j = 0, ct = 0; j < csvData.length; j++)
                        {
                           String csvString = csvData[j].trim();
                           String header = headerNames[j].toLowerCase().trim();

                           if (csvString.toLowerCase().contains("http"))
                           {
                              if (ct < 2)
                              {
                                 urlVal =
                                    urlVal + csvString.replace("[", "").replace("]", "") + "\n";
                                 ct++;
                              }
                           }
                           else if ((!csvString.equals("[]") && header.contains("compare"))
                              && incr == 0)
                           {
                              DefectSetterAndGetter defMap = new DefectSetterAndGetter();
                              csvString = csvString.replace("[", "").replace("]", "");
                              defMap.setDefectDesc(csvString.toLowerCase());
                              defMap.setDefectUrl(urlVal + tokenSeparator + bookRef
                                 + tokenSeparator + testDataVal);
                              defFailedList.add(defMap);
                              list.setDefList(defFailedList);
                              testCaseIdFailMap.put(testCaseId, list);
                              compare = false;
                           }

                           else if ((!csvData[j].equals("[]") && header.contains("compare"))
                              && incr != 0)
                           {
                              String[] csvStringArray = csvData[j].split("\\*+");
                              for (int l = 0; l < csvStringArray.length; l++)
                              {
                                 csvString =
                                    csvStringArray[l].replace("[", "").replace("]", "")
                                       .toLowerCase().trim();
                                 LinkedHashMap<String, DefectList> testCaseIdMap1 =
                                    searchTextInMap(testCaseIdFailMap, testCaseId, csvString,
                                       urlVal, bookRef, testDataVal);
                                 testCaseIdFailMap.putAll(testCaseIdMap1);
                              }
                              compare = false;
                           }

                           if (compare && j == (csvData.length - 1))
                           {
                              DefectSetterAndGetter defMap = new DefectSetterAndGetter();
                              defMap.setDefectDesc(scenarioName);
                              defMap.setDefectUrl("Passed" + tokenSeparator + "Passed"
                                 + tokenSeparator + urlVal + "" + tokenSeparator + "");
                              defPassedList.add(defMap);
                              passList.setDefList(defPassedList);
                              testCaseIdPassMap.put(testCaseId, passList);
                           }
                        }
                        incr++;
                     }
                     catch (Exception e)
                     {
                     }
                  }
               }
               catch (Exception e)
               {
               }
            }
            Iterator<String> testCaseIdIterator = testCaseIdFailMap.keySet().iterator();
            while (testCaseIdIterator.hasNext())
            {
               String testCaseIdval = testCaseIdIterator.next();
               if (testCaseIdPassMap.containsKey(testCaseIdval))
               {
                  testCaseIdPassMap.remove(testCaseIdval);
               }
            }

            if (c == 0 && enter)
            {
               tstCaseObj.setTestCase(testCaseIdFailMap);
               tstCasePassObj.setTestCase(testCaseIdPassMap);
               tstCaseList.add(tstCaseObj);
               tstCaseList.add(tstCasePassObj);
               scenarioMap.put(scenarioName, tstCaseList);
            }
            else if (enter)
            {

               TestCase tst1 =
                  searchScenariosInExistingMap(scenarioName, scenarioMap, testCaseIdFailMap, 0);
               TestCase tst2 =
                  searchScenariosInExistingMap(scenarioName, scenarioMap, testCaseIdPassMap, 1);
               tstCaseList.add(tst1);
               tstCaseList.add(tst2);
               scenarioMap.put(scenarioName, tstCaseList);
            }
            reader.close();
         }
         catch (FileNotFoundException e)
         {
            e.printStackTrace();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         finally
         {
            try
            {
               if (reader != null)
                  reader.close();
            }
            catch (IOException e)
            {
               e.printStackTrace();
            }
         }
      }
      scenarioObject.setScenario(scenarioMap);
      return scenarioObject;
   }

   /**
    * <p>
    * The method to read the files from given folder
    * </p>
    * 
    * @param flderPath
    * @return List of files
    */
   private List<File> readFolder(String flderPath)
   {
      File file = new File(flderPath);
      List<File> filePathList = new ArrayList<File>();
      if (file.exists())
      {
         File[] arrayOfFiles = file.listFiles();
         for (File individualFile : arrayOfFiles)
         {
            String fileName = individualFile.getName().toLowerCase();
            if (!individualFile.isDirectory()
               && (fileName.contains("final") || fileName.contains("corrected")))
            {
               if (fileName.endsWith(".csv"))
                  filePathList.add(individualFile);
            }
         }
      }
      else
      {
         System.out.println("The give path doesn'exist in the machine:");
      }
      return filePathList;
   }

   /**
    * <p>
    * Main methods start here
    * </p>
    * 
    * @param args
    */
   public void createDashBoard()
   {

      String userName = "", machineIP = "", executionDate = "", envDetails = "";
      SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
      brandObject = new BrandMapping();
      Map<String, Scenario> brandStore = new LinkedHashMap<String, Scenario>();

      for (Map.Entry<String, String> entrySet : brandMap.entrySet())
      {
         String flderPath = entrySet.getValue();
         String brandName = entrySet.getKey().toUpperCase().trim();
         List<File> csvFile = readFolder(flderPath);
         if (csvFile.size() != 0)
         {
            Scenario scenarioObj = processCSVFiles(csvFile);
            Map<String, ArrayList<TestCase>> scenarioTestCaseMap = scenarioObj.getSecnario();
            if (scenarioTestCaseMap.size() != 0)
               brandStore.put(brandName, scenarioObj);
            else
               System.out
                  .println("There is no data in the csv file hence dashboard will not be created");
         }
      }
      brandObject.setBrands(brandStore);
      if (brandStore.size() != 0)
      {
         String defectContent = defectFormation(brandObject);
         writeToTextFIle(defectContent);

         try
         {
            userName = System.getProperty("user.name");

            InetAddress ipAddr = InetAddress.getLocalHost();
            machineIP = ipAddr.getHostAddress();

            Date sysDate = new Date();
            executionDate = fmt.format(sysDate);

         }
         catch (Exception e)
         {
            e.printStackTrace();
         }

         createXmlFile();
         addDetails(userName, executionDate, machineIP, envDetails, brandObject);
         transformXmlToHtml("src"+File.separator+"reportXSL_v1.xsl");
      }
   }
}
