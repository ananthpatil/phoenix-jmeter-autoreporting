package com.jmeter;

import java.util.*;

public class Scenario
{
   private Map<String, ArrayList<TestCase>> scenarioMap;

   /**
    * <p>
    * The method to set the Test Case ID.
    * </p>
    * 
    * @param testCaseMap
    */
   public void setScenario(Map<String, ArrayList<TestCase>> scenarioMap)
   {
      this.scenarioMap = scenarioMap;
   }

   /**
    * <p>
    * The method to get the list of test cases store in the MAP.
    * </p>
    * 
    * @return
    */
   public Map<String, ArrayList<TestCase>> getSecnario()
   {
      return scenarioMap;
   }

}
