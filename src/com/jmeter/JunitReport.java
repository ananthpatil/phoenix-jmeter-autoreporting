package com.jmeter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class JunitReport extends JmeterReport_v1
{

   private String JUNIT_XMLFILEPATH;

   JunitReport(Map<String, String> brandMap, String ouputFolderPath)
   {
      super(brandMap, ouputFolderPath);
   }

   public void addJunitDetails()
   {
      try
      {
         Map<String, Scenario> brandStoredMap = brandObject.getBrands();
         if (brandStoredMap.size() != 0)
         {
            String fileName;
            JUNIT_XMLFILEPATH = outputFolderPath;
            File filVal = new File(JUNIT_XMLFILEPATH);
            fileName = "JUNIT_" + filVal.getName().replaceAll("\\.\\w+", "") + ".xml";
            JUNIT_XMLFILEPATH = JUNIT_XMLFILEPATH.replace(filVal.getName().trim(), fileName);
            createXmlFile();
            addJunitSummary();
         }
         else
         {
            System.out.println("There is no data to create the JUNIT file");
         }
      }
      catch (Exception e)
      {
         System.out.println("Exception in adding the details to Junit");
      }
   }

   /**
    * The method to create the JUNIT XML file
    * 
    * @author ANANT
    */
   private void createXmlFile()
   {
      try
      {
         /*
          * String path = getFileName();
          * 
          * String xmlFileName = System.getProperty("user.dir") + File.separator +
          * "JUNIT_"+fileName+".xml"; setJunitPath(xmlFileName);
          */

         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         Document doc = builder.newDocument();

         Element root = doc.createElement("testsuites");
         doc.appendChild(root);

         Attr attr = doc.createAttribute("name");
         attr.setValue("Automation Execution");
         root.setAttributeNode(attr);

         TransformerFactory tFatcory = TransformerFactory.newInstance();
         Transformer trans = tFatcory.newTransformer();
         DOMSource src = new DOMSource(doc);

         File file = new File(JUNIT_XMLFILEPATH);
         if (file.exists())
         {
            file.delete();
            System.out.println("The existing file deleted by this name:" + JUNIT_XMLFILEPATH);
         }

         StreamResult result = new StreamResult(new File(JUNIT_XMLFILEPATH));
         trans.transform(src, result);
         System.out.println("New JUNIT XML File created :" + JUNIT_XMLFILEPATH + " !");

      }
      catch (ParserConfigurationException e1)
      {
         System.out.println("The parseConfiguration exception:" + e1);
      }
      catch (TransformerConfigurationException e2)
      {
         System.out.println("The Transformer Configuration error:" + e2);
      }
      catch (TransformerException e3)
      {
         System.out.println("TransfomerException:" + e3);
      }
   }

   /**
    * <p>
    * This method add the report data in XML file data.
    * 
    * @author Anant
    *         </p>
    */
   public void addJunitSummary()
   {
      try
      {
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         // String path = getJunitPath();
         File file = new File(JUNIT_XMLFILEPATH);
         float passed = 0, failed = 0, total = 0;

         if (file.exists())
         {
            Document doc = builder.parse(file);
            Element root = doc.getDocumentElement();

            Element testsuite = doc.createElement("testsuite");
            root.appendChild(testsuite);

            Attr attr = doc.createAttribute("name");
            attr.setValue("Unit Test Report");
            testsuite.setAttributeNode(attr);

            Attr tests = doc.createAttribute("tests");
            tests.setValue(String.valueOf(total));
            testsuite.setAttributeNode(tests);

            Attr failure = doc.createAttribute("failures");
            failure.setValue(String.valueOf(failed));
            testsuite.setAttributeNode(failure);

            for (Map.Entry<String, Scenario> brandStoredMap : brandObject.getBrands().entrySet())
            {
               Scenario scenObject = brandStoredMap.getValue();
               for (Map.Entry<String, ArrayList<TestCase>> scenarioMap : scenObject.getSecnario()
                  .entrySet())
               {
                  try
                  {
                     @SuppressWarnings("unused")
                     String scenarioName = scenarioMap.getKey().replace(".csv", "");
                     ArrayList<TestCase> testCaseList = scenarioMap.getValue();

                     TestCase failTstObj = testCaseList.get(0);
                     TestCase passTstObj = testCaseList.get(1);

                     LinkedHashMap<String, DefectList> passMap = passTstObj.getTestCase();
                     LinkedHashMap<String, DefectList> failMap = failTstObj.getTestCase();

                     Iterator<String> passIterator = passMap.keySet().iterator();
                     while (passIterator.hasNext())
                     {
                        String testCaseID = passIterator.next().toLowerCase().trim();

                        Element testcase = doc.createElement("testcase");
                        testsuite.appendChild(testcase);

                        Attr classname = doc.createAttribute("classname");
                        classname.setValue(scenarioName);
                        testcase.setAttributeNode(classname);

                        Attr tstID = doc.createAttribute("id");
                        tstID.setValue(testCaseID);
                        testcase.setAttributeNode(tstID);

                        Attr name = doc.createAttribute("name");
                        name.setValue(testCaseID+">>"+scenarioName);
                        testcase.setAttributeNode(name);

                        Attr time = doc.createAttribute("time");
                        time.setValue("0.0");
                        testcase.setAttributeNode(time);
                        passed++;
                     }

                     Iterator<String> failIterator = failMap.keySet().iterator();
                     while (failIterator.hasNext())
                     {
                        String testCaseID = failIterator.next().toLowerCase().trim();

                        Element testcase = doc.createElement("testcase");
                        testsuite.appendChild(testcase);

                        Attr classname = doc.createAttribute("classname");
                        classname.setValue(scenarioName);
                        testcase.setAttributeNode(classname);

                        Attr tstID = doc.createAttribute("id");
                        tstID.setValue(testCaseID);
                        testcase.setAttributeNode(tstID);

                        Attr name = doc.createAttribute("name");
                        name.setValue(testCaseID+">>"+scenarioName);
                        testcase.setAttributeNode(name);

                        Attr time = doc.createAttribute("time");
                        time.setValue("0.0");
                        testcase.setAttributeNode(time);

                        Element fail = doc.createElement("failure");
                        testcase.appendChild(fail);

                        Attr message = doc.createAttribute("message");
                        message.setValue("test failure");
                        fail.setAttributeNode(message);
                        fail.setTextContent(testCaseID + " Verification failed");
                        failed++;
                     }
                  }

                  catch (Exception e)
                  {
                     System.out.println("Exception while adding the Test case "
                        + "detail to existing xml file:" + e);
                  }
               }
            }

            total = passed + failed;
            failure.setValue(String.valueOf(failed));
            tests.setValue(String.valueOf(total));
            TransformerFactory tFatcory = TransformerFactory.newInstance();
            Transformer trans = tFatcory.newTransformer();
            DOMSource src = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(JUNIT_XMLFILEPATH));
            trans.transform(src, result);
            System.out.println("Junit file updated and added summary successfully");
         }
      }
      catch (SAXException e1)
      {
         System.out.println("The Sax error:" + e1);
      }
      catch (IOException e2)
      {
         System.out.println("The I/O error while adding the summary" + e2);
      }
      catch (ParserConfigurationException e3)
      {
         System.out.println("The parser error while adding summary:" + e3);
      }
      catch (TransformerConfigurationException e4)
      {
         System.out.println("The transformer configuration error while adding the summary:" + e4);
      }
      catch (TransformerException e5)
      {
         System.out.println("The TransformerException  error while adding the summary:" + e5);
      }
   }
}
