package com.jmeter;

import java.io.File;
import java.util.*;

public class JmeterDashBoard
{

   public static void main(String[] args)
   {

      String flderPath, ouputFolderPath;
      Map<String, String> brandMap = new LinkedHashMap<String, String>();

      /*
       * flderPath = "C:\\My_Work\\apache-jmeter-2.13\\bin\\Com_CS\\"; txtFilePath =
       * "C:\\My_Work\\Defect_TH" ;
       */

      flderPath = "C:\\Users\\t13390\\Desktop\\results1\\phoenix\\hybris\\nordics\\web\\";
      ouputFolderPath = "C:\\MyWork\\DefectSummary";

      /*
       * JunitReport report = new JunitReport(); report.createDashBoard(flderPath, txtFilePath);
       * report.addJunitDetails();
       */

      File fldPath = new File(flderPath);
      File filVal[] = fldPath.listFiles();
      for (File fileName : filVal)
      {
         if (fileName.isDirectory())
         {
            File[] subFile = fileName.listFiles();
            for (File subFlName : subFile)
            {
               String subPath = subFlName.toString() + File.separator + "results";
               File rsltPath = new File(subPath);
               if (rsltPath.exists())
                  brandMap.put(subFlName.getName(), rsltPath.toString());
               else
                  System.out.println("results folder is not there in this path:" + subPath);
            }
         }
      }
      JunitReport report = new JunitReport(brandMap, ouputFolderPath);
      report.createDashBoard();
      report.addJunitDetails();

   }
}
