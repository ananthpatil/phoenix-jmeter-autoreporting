package com.jmeter;

import java.util.Map;

public class BrandMapping
{
   private Map<String, Scenario> brandMap;

   /**
    * <p>
    * The method to set the Test Case ID.
    * </p>
    * 
    * @param testCaseMap
    */
   public void setBrands(Map<String, Scenario> brandMap)
   {
      this.brandMap = brandMap;
   }

   /**
    * <p>
    * The method to get the list of test cases store in the MAP.
    * </p>
    * 
    * @return
    */
   public Map<String, Scenario> getBrands()
   {
      return brandMap;
   }
}
