package com.jmeter;

import java.util.*;

public class TestCase
{
   private LinkedHashMap<String, DefectList> testCaseMap;

   /**
    * <p>
    * The method to set the Test Case ID.
    * </p>
    * 
    * @param testCaseMap
    */
   public void setTestCase(LinkedHashMap<String, DefectList> testCaseMap)
   {
      this.testCaseMap = testCaseMap;
   }

   /**
    * <p>
    * The method to get the list of test cases store in the MAP.
    * </p>
    * 
    * @return
    */
   public LinkedHashMap<String, DefectList> getTestCase()
   {
      return testCaseMap;
   }
}
