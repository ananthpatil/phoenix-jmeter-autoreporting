package com.jmeter;

import java.io.*;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

public class JmeterReport
{

   private String xmlFileName;

   public String tokenSeparator = "^^";

   public String getFileName()
   {
      return xmlFileName;
   }

   public void setFileName(String xmlFileName)
   {
      this.xmlFileName = xmlFileName;
   }

   /**
    * The following method used To create the XMl file
    * 
    * @return void
    */
   public void createXmlFile()
   {
      try
      {
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         Document doc = builder.newDocument();

         Element root = doc.createElement("SummaryResult");
         doc.appendChild(root);

         TransformerFactory tFatcory = TransformerFactory.newInstance();
         Transformer trans = tFatcory.newTransformer();
         DOMSource src = new DOMSource(doc);

         String xmlFilName = System.getProperty("user.dir") + File.separator + getFileName();
         xmlFilName = xmlFilName + ".xml";

         File file = new File(xmlFilName);
         if (file.exists())
            file.delete();
         StreamResult result = new StreamResult(new File(xmlFilName));
         trans.transform(src, result);
         System.out.println("New XML File created :" + xmlFilName + " !");
      }
      catch (ParserConfigurationException e1)
      {
         System.out.println("The parseConfiguration exception:" + e1);
      }
      catch (TransformerConfigurationException e2)
      {
         System.out.println("The Transformer Configuration error:" + e2);
      }
      catch (TransformerException e3)
      {
         System.out.println("TransfomerException:" + e3);
      }
   }

   /**
    * <p>
    * The following method is used to add the defect details to XML
    * </p>
    * 
    * @returns void
    */
   private void addDetails(String userName, String currentDate, String machineIP,
      String envDetails, Map<String, LinkedHashMap<String, String>> scenarioDefectMap)
   {
      String summryDesc = " mismatch is happening from the ", xmlFileNamae;
      String summaryCompDesc = " component verification failed from ";

      try
      {
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         DocumentBuilder builder = factory.newDocumentBuilder();
         xmlFileNamae = System.getProperty("user.dir") + File.separator + getFileName();
         xmlFileNamae = xmlFileNamae + ".xml";
         File fin = new File(xmlFileNamae);
         if (fin.exists())
         {
            Document doc = builder.parse(fin);
            Element root = doc.getDocumentElement();

            Element generalInfo = doc.createElement("GeneralInfo");
            root.appendChild(generalInfo);

            Element user = doc.createElement("UserName");
            generalInfo.appendChild(user);
            user.setTextContent(userName);

            Element dateExecute = doc.createElement("DateOfExecution");
            generalInfo.appendChild(dateExecute);
            dateExecute.setTextContent(currentDate);

            Element ip = doc.createElement("MachineIP");
            generalInfo.appendChild(ip);
            ip.setTextContent(machineIP);

            Element env = doc.createElement("Environment");
            generalInfo.appendChild(env);
            env.setTextContent(envDetails);

            for (Map.Entry<String, LinkedHashMap<String, String>> scenarioMap : scenarioDefectMap
               .entrySet())
            {
               String scenarioName = scenarioMap.getKey().replace(".csv", "");
               Map<String, String> defectMap = scenarioMap.getValue();
               for (Map.Entry<String, String> defMap : defectMap.entrySet())
               {
                  String compName = null, pageName1 = "", pageName2 = "", expected = null, actual =
                     null, defectName, urlToLog;
                  String stepsTpRep = "";
                  defectName = defMap.getKey().toLowerCase().trim();
                  urlToLog = defMap.getValue().trim();
                  boolean reportFlag = true;

                  if (!defectName.contains("=") && urlToLog.contains(tokenSeparator))
                  {
                     try
                     {
                        String[] infoArray = urlToLog.split("\\^\\^");
                        compName = defectName.toLowerCase().trim();
                        try
                        {
                           pageName1 =
                              compName.substring(0, compName.lastIndexOf("_")).replaceAll(
                                 "\\w*\\_", "");
                        }
                        catch (Exception e)
                        {
                        }
                        compName = compName.replaceAll("\\w*\\_", "");
                        expected = infoArray[0].toLowerCase().trim();
                        actual = infoArray[1].toLowerCase().trim();
                        urlToLog = infoArray[2].trim();
                        summryDesc = " component verification failed from ";
                        stepsTpRep =
                           stepsTpRep + "1.Navigate to the " + pageName1 + " page check the "
                              + compName + " value";

                     }
                     catch (Exception e)
                     {
                        reportFlag = false;
                     }
                  }
                  else
                  {
                     try
                     {
                        compName =
                           defectName.substring(0, defectName.indexOf(" in ")).trim()
                              .replaceAll("\\w*\\_", "");
                        pageName2 =
                           defectName.substring(defectName.lastIndexOf(" in ") + 3,
                              defectName.lastIndexOf("=")).trim();
                        pageName1 =
                           defectName.substring(defectName.indexOf(" in ") + 3,
                              defectName.indexOf("=")).trim();
                        expected =
                           defectName.substring(defectName.indexOf("=") + 1,
                              defectName.indexOf("|"));
                        actual = defectName.substring(defectName.lastIndexOf("=") + 1);
                        actual = actual.replaceAll("\\*+", "");
                        expected = expected.replaceAll("\\*+", "");
                        summryDesc = " mismatch is happening from the ";
                        stepsTpRep =
                           stepsTpRep + "1.Navigate to the " + pageName1 + " page check the "
                              + compName + " value \r\r\n" + "2.Navigate to the " + pageName2
                              + " page check the " + compName + " value";

                     }
                     catch (Exception e)
                     {
                        reportFlag = false;
                     }
                  }
                  if (reportFlag)
                  {
                     Element defectSummary = doc.createElement("DefectSummary");
                     root.appendChild(defectSummary);

                     Element secn = doc.createElement("ScenarioName");
                     secn.setTextContent(scenarioName);
                     defectSummary.appendChild(secn);

                     Element summary = doc.createElement("Summary");
                     summary.setTextContent(compName + summryDesc + pageName1 + " to " + pageName2
                        + " page");
                     defectSummary.appendChild(summary);

                     Element urlVal = doc.createElement("URL");
                     urlVal.setTextContent(urlToLog);
                     defectSummary.appendChild(urlVal);

                     Element steps = doc.createElement("StepsToReproduce");
                     steps.setTextContent(stepsTpRep);
                     defectSummary.appendChild(steps);

                     Element expectedTag = doc.createElement("Expected");
                     expectedTag.setTextContent(expected);
                     defectSummary.appendChild(expectedTag);

                     Element actualTag = doc.createElement("Actual");
                     actualTag.setTextContent(actual);
                     defectSummary.appendChild(actualTag);
                  }
               }
            }
            TransformerFactory tfactory = TransformerFactory.newInstance();
            Transformer trans = tfactory.newTransformer();
            DOMSource src = new DOMSource(doc);
            StreamResult result = new StreamResult(fin);
            trans.transform(src, result);
         }
      }
      catch (ParserConfigurationException e)
      {
         System.out.println("ParseConfiguration Error:" + e);
      }
      catch (SAXException e1)
      {
         System.out.println("SAX error:" + e1);
      }
      catch (IOException e2)
      {
         System.out.println("The I/O error:" + e2);
      }
      catch (TransformerConfigurationException e)
      {
         e.printStackTrace();
      }
      catch (TransformerException e3)
      {
         System.out.println("Transformer Exception:" + e3);
      }
   }

   /**
    * <p>
    * The method is used to convert the XML to HTML
    * </p>
    * 
    * @param xslFile
    * @return void
    */
   private void transormXmlToHtml(String xslFile)
   {
      try
      {
         System.out.println("the file:" + xslFile);
         String path = System.getProperty("user.dir") + File.separator;
         File dltHtmlFile = new File(path + getFileName() + ".html");
         if (dltHtmlFile.exists())
         {
            if (dltHtmlFile.getName().contains(".html"))
            {
               dltHtmlFile.delete();
            }
         }
         TransformerFactory tfactory = TransformerFactory.newInstance();
         StreamSource xmlfile = new StreamSource(path + getFileName() + ".xml");
         StreamSource xslfile = new StreamSource(xslFile);
         FileOutputStream htmlfile = new FileOutputStream(path + getFileName() + ".html");
         Transformer trans = tfactory.newTransformer(xslfile);
         trans.transform(xmlfile, new StreamResult(htmlfile));

      }
      catch (TransformerConfigurationException e1)
      {
         System.out.println("The TransformerConfigurationException:" + e1);
      }
      catch (FileNotFoundException e2)
      {
         System.out.println("The FileNotFoundException:" + e2);
      }
      catch (TransformerException e3)
      {
         System.out.println("The TransformerException:" + e3);
      }
   }

   /**
    * The method used to form the defect string
    * 
    * @param scenarioDefectMap
    * @return String
    */
   private String defectFormation(Map<String, LinkedHashMap<String, String>> scenarioDefectMap)
   {
      String scenarioName, defectName, urlToLog, pageName1, pageName2, compName, actual, expected, url, bookref;
      String summryDesc = " mismatch is happening from the ";
      String summaryCompDesc = " component verification failed from ";

      StringBuilder builder = new StringBuilder();
      for (Map.Entry<String, LinkedHashMap<String, String>> scenarioMap : scenarioDefectMap
         .entrySet())
      {
         scenarioName = scenarioMap.getKey().replace(".csv", "");
         Map<String, String> defMap = scenarioMap.getValue();
         builder.append("==========================SCENARIO STARTS HERE=======================\n");
         for (Map.Entry<String, String> defectMap : defMap.entrySet())
         {
            pageName1 = "";
            pageName2 = "";
            compName = "";
            bookref = "";

            defectName = defectMap.getKey().toLowerCase().trim();
            urlToLog = defectMap.getValue().trim();

            if (!defectName.contains("=") && urlToLog.contains(tokenSeparator))
            {
               try
               {
                  String[] infoArray = urlToLog.split("\\^\\^");
                  compName = defectName.toLowerCase().trim();
                  try
                  {
                     pageName1 =
                        compName.substring(0, compName.lastIndexOf("_")).replaceAll("\\w*\\_", "");
                  }
                  catch (Exception e)
                  {
                  }
                  compName = compName.replaceAll("\\w*\\_", "");
                  expected = infoArray[0].toLowerCase().trim();
                  actual = infoArray[1].toLowerCase().trim();
                  url = infoArray[2].trim();
                  bookref = infoArray[3].trim();

                  builder.append("SenarioName:" + scenarioName + "\n");
                  builder.append("Summary:" + compName + summaryCompDesc + pageName1 + " page\n");
                  builder.append("Steps to Reproduce:-" + "\n");
                  builder.append("1.Launch the below URL" + "\n" + url + "\n");
                  builder.append("2.Navigate to the " + pageName1 + " page check the " + compName
                     + " value\n");
                  builder.append("Expected Result:" + expected + "\n");
                  builder.append("Actual Result:" + actual + "\n");
                  builder.append("BookingRef:" + bookref + "\n");
                  builder.append("\r\n");
               }
               catch (Exception e)
               {
                  System.out.println("Exception in defectFormation method:" + e);
               }
            }
            else
            {
               try
               {
                  compName =
                     defectName.substring(0, defectName.indexOf(" in ")).trim()
                        .replaceAll("\\w*\\_", "");
                  pageName2 =
                     defectName.substring(defectName.lastIndexOf(" in ") + 3,
                        defectName.lastIndexOf("=")).trim();
                  pageName1 =
                     defectName.substring(defectName.indexOf(" in ") + 3, defectName.indexOf("="))
                        .trim();
                  expected =
                     defectName.substring(defectName.indexOf("=") + 1, defectName.indexOf("|"));
                  actual = defectName.substring(defectName.lastIndexOf("=") + 1);
                  actual = actual.replaceAll("\\*+", "");
                  expected = expected.replaceAll("\\*+", "");

                  builder.append("SenarioName:" + scenarioName + "\n");
                  builder.append("Summary:" + compName + summryDesc + pageName1 + " to "
                     + pageName2 + " page\n");
                  builder.append("Steps to Reproduce:-" + "\n");
                  builder.append("1.Launch the below URL" + "\n" + urlToLog + "\n");
                  builder.append("2.Navigate to the " + pageName1 + " page check the " + compName
                     + " value\n");
                  builder.append("3.Navigate to the " + pageName2 + " page check the " + compName
                     + " value\n");
                  builder.append("Expected Result:" + expected + "\n");
                  builder.append("Actual Result:" + actual + "\n");
                  builder.append("\r\n");
               }
               catch (Exception e)
               {
                  System.out.println("Exception in defectFormation method:" + e);
               }
            }
         }
         builder.append("==========================SCENARIO ENDS HERE======================\n");
      }
      return builder.toString();
   }

   /**
    * <p>
    * The method to write the defect string to Text file
    * </p>
    * 
    * @param fileName
    * @param defectString
    */
   private void writeToTextFIle(String fileName, String defectString)
   {
      BufferedWriter writer = null;
      try
      {
         fileName = System.getProperty("user.dir") + File.separator + fileName + ".txt";
         writer = new BufferedWriter(new FileWriter(fileName));
         writer.write(defectString);
         writer.close();
      }
      catch (Exception e)
      {
         System.out.println("Error occured while writting into text file:" + e);
      }
      finally
      {
         try
         {
            if (writer != null)
               writer.close();
         }
         catch (Exception e)
         {
         }
      }
   }

   /**
    * <p>
    * The method used to search the New defect details In map
    * </p>
    * 
    * @param storedDefectMap
    * @param csvString
    * @return boolean
    */
   private boolean searchTextInMap(Map<String, String> storedDefectMap, String csvString)
   {
      boolean flag = false;
      for (Map.Entry<String, String> entry : storedDefectMap.entrySet())
      {
         String storedKey = entry.getKey();
         try
         {
            if (storedKey.contains(csvString.substring(0, csvString.indexOf("="))))
            {
               flag = true;
               break;
            }
         }
         catch (Exception e)
         {
         }
      }
      return flag;
   }

   /**
    * <p>
    * The method used to search the New defect details In map
    * </p>
    * 
    * @param storedDefectMap
    * @param csvString
    * @return boolean
    */
   private boolean searchTextInMapForComp(Map<String, String> storedDefectMap, String csvString)
   {
      boolean flag = false;
      for (Map.Entry<String, String> entry : storedDefectMap.entrySet())
      {
         String storedKey = entry.getKey();
         try
         {
            if (storedKey.contains(csvString))
            {
               flag = true;
               break;
            }
         }
         catch (Exception e)
         {
         }
      }
      return flag;
   }

   /**
    * <p>
    * The method used extract the unique defect failures from CSV file
    * </p>
    * 
    * @param csvFile
    * @return
    */
   private Map<String, LinkedHashMap<String, String>> processCSVFiles(List<File> csvFile)
   {
      Map<String, LinkedHashMap<String, String>> scenarioMap;
      scenarioMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
      CSVReader reader = null;
      for (int c = 0; c < csvFile.size(); c++)
      {
         try
         {
            String scenarioName, urlVal, headerString, bookRef;
            String[] csvData, headerNames;
            int incr = 0, bookIndex = 0;
            boolean bookFound = false;

            LinkedHashMap<String, String> defectList = new LinkedHashMap<String, String>();
            scenarioName = csvFile.get(c).getName();
            reader = new CSVReader(new FileReader(csvFile.get(c)));
            headerNames = reader.readNext();
            headerString = Arrays.toString(headerNames).toLowerCase().replace(" ", "");
            headerString = headerString.replace("[", "").replace("]", "");

            for (int h = 0; h < headerNames.length; h++)
            {
               String headerName = headerNames[h].trim().toLowerCase();
               headerName = headerName.replace(" ", "");
               if (headerName.contains("bookingref") || headerName.contains("bookref"))
               {
                  bookIndex = h;
                  bookFound = true;
                  break;
               }
            }

            if (headerString.contains("status,actualresult,expectedresult,url,component,filename"))
            {
               while ((csvData = reader.readNext()) != null)
               {
                  bookRef = null;
                  if (incr == 0)
                  {
                     if (csvData[0].trim().equalsIgnoreCase("failed"))
                     {
                        if (bookFound)
                           bookRef = csvData[bookIndex];

                        defectList.put(csvData[4].trim(), csvData[2] + tokenSeparator + csvData[1]
                           + tokenSeparator + csvData[3] + tokenSeparator + bookRef);
                     }
                  }
                  else
                  {
                     boolean flag = searchTextInMapForComp(defectList, csvData[4].trim());
                     if (flag == false)
                        defectList.put(csvData[4].trim(), csvData[2] + tokenSeparator + csvData[1]
                           + tokenSeparator + csvData[3] + tokenSeparator + bookRef);
                  }
               }
            }
            else
            {
               while ((csvData = reader.readNext()) != null)
               {
                  urlVal = "";
                  if (incr == 0)
                  {
                     for (int j = 0, ct = 0; j < 10; j++, ct++)
                     {
                        String csvString = csvData[j];
                        if (csvString.toLowerCase().contains("http"))
                        {
                           if (ct == 0)
                              urlVal = csvString;
                        }
                        else
                        {
                           if (!csvString.equals("[]"))
                           {
                              csvString = csvString.replace("[", "").replace("]", "");
                              defectList.put(csvString, urlVal);
                           }
                        }
                     }
                  }
                  else
                  {
                     for (int j = 0, ct = 0; j < 10; j++, ct++)
                     {
                        if (csvData[j].toLowerCase().contains("http"))
                        {
                           if (ct == 0)
                              urlVal = csvData[j];
                        }
                        else
                        {
                           if (!csvData[j].equals("[]"))
                           {
                              String[] csvStringArray = csvData[j].split("\\*+");
                              for (int l = 0; l < csvStringArray.length; l++)
                              {
                                 String csvString =
                                    csvStringArray[l].replace("[", "").replace("]", "");
                                 boolean flag = searchTextInMap(defectList, csvString);
                                 if (flag == false)
                                    defectList.put(csvString, urlVal);
                              }
                           }
                        }
                     }
                  }
                  incr++;
               }
            }
            scenarioMap.put(scenarioName, defectList);
            reader.close();
         }
         catch (FileNotFoundException e)
         {
            e.printStackTrace();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         finally
         {
            try
            {
               if (reader != null)
                  reader.close();
            }
            catch (IOException e)
            {
               e.printStackTrace();
            }
         }
      }
      return scenarioMap;
   }

   /**
    * <p>
    * The method to read the files from given folder
    * </p>
    * 
    * @param flderPath
    * @return List of files
    */
   private List<File> readFolder(String flderPath)
   {
      File file = new File(flderPath);
      List<File> filePathList = new ArrayList<File>();
      if (file.exists())
      {
         File[] arrayOfFiles = file.listFiles();
         for (File individualFile : arrayOfFiles)
         {
            String fileName = individualFile.getName().toLowerCase();
            if (!individualFile.isDirectory()
               && (fileName.contains("final") || fileName.contains("corrected")))
            {
               if (fileName.endsWith(".csv"))
                  filePathList.add(individualFile);
            }
         }
      }
      else
      {
         System.out.println("The give path doesn'exist in the machine:");
      }
      return filePathList;
   }

   /**
    * <p>
    * Main methods start here
    * </p>
    * 
    * @param args
    */
   public static void main(String[] args)
   {
      Map<String, LinkedHashMap<String, String>> scenarioDefectMap;
      String userName = "", machineIP = "", executionDate = "", envDetails = "";
      SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");

      String flderPath = "C:\\My_Work\\apache-jmeter-2.13\\bin\\Com_CS\\", txtFileName =
         "Defect_TH";
      JmeterReport report = new JmeterReport();

      List<File> csvFile = report.readFolder(flderPath);
      scenarioDefectMap = report.processCSVFiles(csvFile);
      String defectContent = report.defectFormation(scenarioDefectMap);
      report.writeToTextFIle(txtFileName, defectContent);

      try
      {
         userName = System.getProperty("user.name");

         InetAddress ipAddr = InetAddress.getLocalHost();
         machineIP = ipAddr.getHostAddress();

         Date sysDate = new Date();
         executionDate = fmt.format(sysDate);

      }
      catch (Exception e)
      {
         e.printStackTrace();
      }

      report.setFileName(txtFileName);
      report.createXmlFile();
      report.addDetails(userName, executionDate, machineIP, envDetails, scenarioDefectMap);
      report.transormXmlToHtml("src\\reportXSL.xsl");
   }
}
