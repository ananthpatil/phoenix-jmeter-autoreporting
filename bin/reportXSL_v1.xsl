<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/"> 
  <html>
  <head>
   <title>Automation Summary</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <script type='text/javascript' src='http://code.jquery.com/jquery-1.10.1.min.js'></script>
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	<script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js"/>
	<script src='http://cdn.rawgit.com/noelboss/featherlight/1.0.4/release/featherlight.min.js' type='text/javascript' charset='utf-8'></script>
   	<script type='text/javascript'>
   	//<![CDATA[ 
   	
   	google.charts.load('current', {packages: ['corechart']});
   	google.charts.setOnLoadCallback(pieChart);
   	
   	 function pieChart() {
   	   try{
   	 		 var selIndex = document.getElementById('sel1').selectedIndex;
  			 if(selIndex == 0){
               selIndex = 1;
             }
             passVal = document.querySelector('div#summarytablediv'+selIndex+' td:nth-child(1)').innerHTML;
             failVal = document.querySelector('div#summarytablediv'+selIndex+' td:nth-child(2)').innerHTML;
             passVal = parseFloat(passVal);
             failVal = parseFloat(failVal);
            
             var data = new google.visualization.DataTable();
             data.addColumn('string', 'TestStatus');
             data.addColumn('number', 'Count');
             data.addRows([
               ['Pass', passVal],
               ['Fail', failVal]
             ]);
               
            // Set chart options
             var options = {
                backgroundColor: { fill:'transparent' },
                titleTextStyle: { color: '#1366d7', fontSize: '14' },
                title:'Pass/Fail summary',
                width:550,
                height:275,
                fontName: 'Source Sans Pro',
			    fontSize: '12',
                is3D:true,
                colors: ['#00af00', 'red'],
               // chartArea: {'width': '65%', 'height': '65%'}
                pieSliceText: 'value'
             };
            
            // Instantiate and draw the chart.
            var chart = new google.visualization.PieChart(document.getElementById('piechart'+selIndex));
            chart.draw(data, options);
          }catch(err){
             console.log('exception in pie chart:'+err.message);
           }
        }

    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(barChart);
    function barChart() {
     try{
        var selIndex = document.getElementById('sel1').selectedIndex;
  	    if(selIndex == 0){
          selIndex = 1;
        }
        passVal = document.querySelector('div#summarytablediv'+selIndex+' td:nth-child(1)').innerHTML;
        failVal = document.querySelector('div#summarytablediv'+selIndex+' td:nth-child(2)').innerHTML;
        passVal = parseInt(passVal);
        failVal = parseInt(failVal);
     
        var data = google.visualization.arrayToDataTable([
         ["TestStatus", "Count", { role: "style" } ],
         ["Pass", passVal, "#00af00"],
         ["Fail", failVal, "red"],
         ["Info", 1, "#7570b3"]
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

        var options = {
          title: "Pass/Fail summay of Automation Testing",
          titleTextStyle: { color: '#1366d7', fontSize: '14' },
          backgroundColor: { fill:'transparent' },
          width: 600,
          height: 300,
          fontSize: '12',
          bar: {groupWidth: "30%"},
          legend: { position: "none" },
        };
      
        var selIndex = document.getElementById('sel1').selectedIndex;
  	    if(selIndex == 0){
             selIndex = 1;
        }
       var chart = new google.visualization.ColumnChart(document.getElementById("barchart"+selIndex));
       chart.draw(view, options);
      }catch(err){
            console.log('exception in bar chart:'+err.message);
       }
     }

   	
      function myFunction(defectid,deftableid) {
  			var input, searchValue, table, tr, td1,td2, i;
  			var selIndex = document.getElementById('sel1').selectedIndex;
  			if(selIndex == 0){
                 selIndex = 1;
            }
  			input = document.getElementById(defectid+selIndex);
  			searchValue = input.value.toUpperCase();
  			table = document.getElementById(deftableid+selIndex);
  			tr = table.getElementsByTagName('tr');
  			for (i = 0; i < tr.length; i++) {
    		td1 = tr[i].getElementsByTagName("td")[0];
    		td2 = tr[i].getElementsByTagName("td")[1];
    		if (td1 || td2) {
              if (td1.innerHTML.toUpperCase().indexOf(searchValue) > -1||
                  td2.innerHTML.toUpperCase().indexOf(searchValue) > -1) {
                 tr[i].style.display = "";
              } else {
                 tr[i].style.display = "none";
              }
           }       
        }
      }
 
     function myFunction1(id1,id2) {
  			var input, searchValue, table, tr, td1,td2, i;
  			var selIndex = document.getElementById('sel1').selectedIndex;
  			if(selIndex == 0){
                 selIndex = 1;
            }
            input = document.getElementById(id1+selIndex);
  			searchValue = input.value.toUpperCase();
  			table = document.getElementById(id2+selIndex);
  			tr = table.getElementsByTagName('tr');
  			for (i = 0; i < tr.length; i++) {
    		td1 = tr[i].getElementsByTagName("td")[1];
    		td2 = tr[i].getElementsByTagName("td")[2];
    		if (td1 || td2) {
              if (td1.innerHTML.toUpperCase().indexOf(searchValue) > -1||
                  td2.innerHTML.toUpperCase().indexOf(searchValue) > -1) {
                 tr[i].style.display = "";
              } else {
                 tr[i].style.display = "none";
              }
           }       
        }
      }
      
     function deleteRow(r,deftableid) {
           var selIndex = document.getElementById('sel1').selectedIndex;
  		   if(selIndex == 0){
               selIndex = 1;
           }
           var row = r.parentNode.parentNode.rowIndex;
           document.getElementById(deftableid+selIndex).deleteRow(row);
     }
     
     function deleteRow1(r,passtableid) {
           var selIndex = document.getElementById('sel1').selectedIndex;
  		   if(selIndex == 0){
               selIndex = 1;
           }
           var row = r.parentNode.parentNode.rowIndex;
           document.getElementById(passtableid+selIndex).deleteRow(row);
     }
     
    
     function summaryLink(class1,class2,class3,class4){
         var selIndex = document.getElementById('sel1').selectedIndex;
         var element1 = document.getElementsByClassName(class1);
         if(selIndex > 0){
           selIndex = selIndex - 1;
         }
         for(e=0;e<element1.length; e++){
             if(e == selIndex)
               element1[e].style.display = 'block';
             else
                element1[e].style.display = 'none';
         }
         hideElement(class2);
         hideElement(class3);
         hideElement(class4);
      }
      
      function hideElement(classname){
         var element2 = document.getElementsByClassName(classname);
         for(e=0;e<element2.length;e++){
            if(element2[e].style.display == 'block' || element2[e].style.display == 'inline')
                element2[e].style.display = 'none';
         }
      }
 
      
     function dropdownValidation(element){
        var drpIndex = element.selectedIndex;
         if(drpIndex == 0)
            drpIndex = 1;       
        var classArray = ['defectClass', 'passClass', 'tstClass','summaryClass'];
        for(var a=0;a<classArray.length;a++){
            var element = document.getElementsByClassName(classArray[a]);
            var flag = false;
            for(var e=0;e<element.length;e++){
              if((element[e].style.display == 'block' || element[e].style.display == 'inline') 
                     && flag == false){
                   flag = true;
                   e=0;
               }
             if(flag == true) {   
               if(e == (drpIndex-1)){
                 google.charts.setOnLoadCallback(pieChart);
                 google.charts.setOnLoadCallback(barChart);
                 element[e].style.display='block';
                }else{
                 element[e].style.display='none';
               }
            }
          }
        }
     }
    
    //]]>
   
    
   </script>
   <style type="text/css">
               table {
                     border-collapse: collapse;
                }

                table, th, td {
                     border: 1px solid black;
                }
                tr:hover {
                     background-color:#eee;
                }
               .margin {
                        margin-top: 0px;
                        padding-bottom: 50px;
                        padding-right: 10px;
                        padding-left: 10px;        
                 }
                th {
                     height: 30px;
                     text-align: center;
                     font-size: 18px;
                     font-family: "Times New Roman", Times, serif;
                     font-style: italic;
                }
				input[id^=defectsearch]{
                  background-image: url('/css/searchicon.png');
				  background-position: 10px 10px;
				  background-repeat: no-repeat;
                  font-size: 16px;
                  padding: 9px 20px 9px 40px;
                  border: 1px solid #ddd;
                  width:35%;
                  border-radius: 8px;
                 <!-- width: 100%;
                  margin-bottom: 12px;
				  margin-top: 5px;-->
                }
                input[id^=passsearch] {
                  background-image: url('/css/searchicon.png');
				  background-position: 10px 10px;
				  background-repeat: no-repeat;
                  font-size: 16px;
                  padding: 9px 20px 9px 40px;
                  border: 1px solid #ddd;
                  width:35%;
                  border-radius: 8px;
                  <!--margin-bottom: 12px;
				  margin-top: 5px;
				  width: 100%; -->
                }
                
                input[id^=tstsearch] {
                  background-image: url('/css/searchicon.png');
				  background-position: 10px 10px;
				  background-repeat: no-repeat;
                  font-size: 16px;
                  padding: 9px 20px 9px 40px;
                  border: 1px solid #ddd;
                  margin-bottom: 12px;
				  margin-top: 5px;
				  border-radius: 8px;
                }
				.div1{
				  height:20px;
				  width:50px;
				  margin:1px,1px;
				  border:1px solid;
				}
				.submit{
				  position: relative;
				  height:30px;
				  width: 80px;
				  margin-left:7px;
				  margin-right:5px;
				  cursor:pointer;
				  border-radius: 8px;
				  border: 1px solid #8e8989;
				}
				
				.tstclass{
				   margin-right:20%;
				   margin-left:20%;
				   max-width:60%;
				   min-width:60%;
				   margin-top:125px;
				}
				div[id^='piechart']
				{
				   <!--padding-left: 55%;-->
                   <!--padding-right: 30%;-->
                   padding-left:10px;
                   padding-right:10px;
                   float:right;
                   max-width:50%;
				
				}
				div[id^='barchart']
				{
				   <!--padding-left: 10%;
                   padding-right: 30%;
                   margin-top:-280px;-->
                   padding-left:10px;
                   padding-right:10px;
                   float:left;
                   max-width:50%;
				
				}
				div[id^='summarytablediv']{
				  max-width: 540px; 
				  min-width: 540px; 
				  padding-left:30%;
				}
				
				h2{
				   margin-left:34%;
				   margin-right:25%;
				}
				
			    .footer {
   				    background-color: #70cbf4;
    				color: white;
                    text-align: right;
                    padding: 12px;
                    margin-top:5px;
                }
               .linktext{
                   background-color: #818a849e;
                   padding: 8px 1px;
    			   text-decoration: none;
    			   width: 125px;
    			   margin-right: 10px;
    			   color:#f9f9f9;
    			   text-align:center;
    			   float:left;
    			   border-radius: 8px;
				  <!-- margin-left: 2px;
    			   margin-bottom: 5px;
    			   margin-top: 8px; --> 
                }
                
                .appUrl{
                   background-color: #dde4de91;
    			   padding: 4px 1px;
    			   text-decoration: none;
    			   width: 75px;
    			   margin: 5px 7px;
				   color: #100f0ee6;
    			   text-align: center;
    			   float: left;
    			   border: 1px solid #8e8989;
				   border-radius: 8px;
                }
                .form-control.custom {
  				   display: block;
  				   clear:both;
  				   margin-top: 45px;
                   padding: 7px 8px;
                   font-size: 14px;
                   color: #f9f9f9;
                   background-color: #818a849e;
                   background-image: none;
                   border: 1px solid #ccc;
                   border-radius: 8px;
                }
                
   </style>
 </head>
   <body style="background-color:#f9f9f9; margin:0 auto;">
   <div style="height:60px; width:100%;  position: fixed; top: 0px; background: #70cbf4;z-index:1;left:0; ">
     <img src="http://static.thomson.co.uk/static-images/_ui/mobile/th/images/logo/TUI-Logo.svg" style="float:left; height:50px;padding:3px"/>
     <h2><u>Automation Summary Report</u></h2>
     <img style="float:right; height:60px; position: relative;top:-71px;left:-20px" src="http://www.sonata-software.com/sites/default/files/favicon.ico" />
   </div>
   <br/>
   
   <div style="padding: 54px 16px 16px 16px;">
     <div style="float: left;" class="linkid">
         <a href='#' class='linktext' onclick="summaryLink('defectClass','passClass','tstClass','summaryClass')">
             DefectReport
        </a>
        <a href='#' class='linktext' onclick="summaryLink('passClass','defectClass','tstClass','summaryClass')">
            PassedReport
        </a>
        <a href='#' class='linktext' onclick="summaryLink('tstClass','defectClass','passClass','summaryClass')">
            TestCaseReport
        </a>
        <a href='#' class='linktext' onclick="summaryLink('summaryClass','defectClass','passClass','tstClass')">
            SummaryReport
        </a>
        <select class="form-control custom" id="sel1" name="mode" onchange="dropdownValidation(this)">
           <option value="">Select Brands</option>
           <xsl:for-each select="SummaryResult/GeneralInfo/Brands"> 
             <option><xsl:value-of select="Brandname"></xsl:value-of></option>
           </xsl:for-each>
        </select>
     </div>
      <table style="float: right;">
       <tr>
         <th>User Name</th>
         <td style="width:50%; padding:5px;">
             <xsl:value-of select="SummaryResult/GeneralInfo/UserName"></xsl:value-of>
         </td>
       </tr>
       <tr>
		 <th>Date Of Execution</th>
		 <td style="width:50%; padding:5px;">
		     <xsl:value-of select="SummaryResult/GeneralInfo/DateOfExecution"></xsl:value-of>
		 </td>
	   </tr>
	   <tr>
         <th>Machine IP</th>
         <td style="width:50%; padding:5px;">
            <xsl:value-of select="SummaryResult/GeneralInfo/MachineIP"></xsl:value-of>
         </td>
       </tr>
       <tr>
         <th>Environment</th>
         <td style="width:50%; padding:5px;">
            <xsl:value-of select="SummaryResult/GeneralInfo/Environment"></xsl:value-of>
         </td>
       </tr>
      </table>
    </div>
   <br/>
   
  <xsl:for-each select="SummaryResult/GeneralInfo/Brands">
  <xsl:variable name="i" select="position()" />
  <xsl:variable name="show1">
  <xsl:choose>
    <xsl:when test="$i=1">
       inline
    </xsl:when>
    <xsl:otherwise>
       none
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  
    <div class="defectClass" style="display:{$show1};left:0;">
     <div style="padding-left: 10px;padding-top: 93px;padding-bottom: 10px">
      <input type="text" id="defectsearch{$i}" onkeyup="myFunction('defectsearch','defectTable')" placeholder="Search by scenario names or test Case ID.." title="Type in a name"/>
     </div>
     <div class="margin">
      <table id="defectTable{$i}" style="width:100%">
      <thead>
	    <tr class="border">
           <th> ScenarioName </th>
           <th> TestCasedID </th>
           <th> Summary  </th>
           <th> URL  </th>
           <th> StepsToReproduce </th>
           <th> ExpectedResult </th>
           <th> ActualResult </th>
           <th> TestData </th>
           <th> Valid/Invalid </th>
         </tr>
	  </thead>
	  <tbody>
	   <xsl:for-each select="defectSummary">
	    <tr>
	       <td >
               <xsl:value-of select="ScenarioName"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="TestCaseID"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="Summary"></xsl:value-of>
	       </td>
	       <td style="word-break: break-all;">
	           <!--<xsl:value-of select="URL"></xsl:value-of>-->
	           <xsl:for-each select="UrlVal/URL">
	              <xsl:variable name="urlVal" select="Value"/>
	              <a href="{$urlVal}" class="appUrl">Click Me</a>
	           </xsl:for-each>
	       </td>
	       <td >
	            <xsl:for-each select="StepsToReproduce/Reproduce">
	                 <xsl:value-of select="Steps"/>
	                 <xsl:element name="br"/>
                </xsl:for-each>
	       </td>
	       <td >
	           <xsl:value-of select="Expected"></xsl:value-of>
	        </td>
	       <td >
	           <xsl:value-of select="Actual"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="TestData"></xsl:value-of>
	       </td>
	       <td >
	          <input class="submit" type="button" value="Delete" onclick="deleteRow(this,'defectTable')"/>
	       </td>
          </tr>
        </xsl:for-each>
       </tbody>
      </table>
     </div>
    </div>
 
  <div class="passClass" style="display:none;">
     <div style="padding-left: 10px;padding-top: 93px;padding-bottom: 10px">
       <input type="text" id="passsearch{$i}" onkeyup="myFunction1('passsearch','tablepassid')" placeholder="Search by scenario names or test Case ID.." title="Type in a name"/>
     </div>
   <div class="margin">
      <table id="tablepassid{$i}" style="width:100%">
      <thead>
	    <tr class="border">
           <th> ScenarioName </th>
           <th> TestCasedID </th>
           <th> Summary  </th>
           <th> URL  </th>
           <th> ExpectedResult </th>
           <th> ActualResult </th>
           <th> Valid/Invalid </th>
         </tr>
	  </thead>
	  <tbody>
	   <xsl:for-each select="PassSummary">  
	    <tr>
	       <td >
	           <xsl:value-of select="ScenarioName"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="TestCaseID"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="Summary"></xsl:value-of>
	       </td>
	       <td style="word-break: break-all;">
	            <xsl:for-each select="UrlVal/URL">
	              <xsl:variable name="urlVal" select="Value"/>
	              <a href="{$urlVal}" class="appUrl">Click Me</a>
	            </xsl:for-each>
	           <!--<a ><xsl:value-of select="URL"></xsl:value-of></a>-->
	       </td>
	       <td >
	           <xsl:value-of select="Expected"></xsl:value-of>
	        </td>
	       <td >
	           <xsl:value-of select="Actual"></xsl:value-of>
	       </td>
	       <td >
	          <input class="submit" type="button" value="Delete" onclick="deleteRow1(this,'tablepassid')"/>
	       </td>
          </tr>
        </xsl:for-each>
       </tbody>
     </table>
    </div>
    </div>
    
  <div style="display:none;" class="tstclass">
   <div>
       <input type="text" id="tstsearch{$i}" onkeyup="myFunction1('tstsearch','tabletstid')" placeholder="Search by status or test Case ID.." title="Type in a name"
       style=" max-width:90%; min-width:90%;"/>
   </div>
   <div>
     <table id="tabletstid{$i}" style=" max-width:90%; min-width:90%; ">
      <thead>
	    <tr class="border">
           <th> Staus Icon </th>
           <th> Status </th>
           <th> TestCaseId  </th>
           <th> ScenarioName  </th>
         </tr>
	  </thead>
	  <tbody>
	   <xsl:for-each select="TestCase">
	     <tr>
	      <xsl:choose>
	       <xsl:when test="Status = 'FAIL'">
	       <td title='FAIL' class='status fail' style="text-align:center;">
	          <i style="color: #eb4549;" class="fa fa-times-circle-o"></i>
	       </td>
	       </xsl:when>
	       <xsl:otherwise>
	          <td title='PASS' class='status pass' style="text-align:center;">
	              <i style="color: #32CD32;" class="fa fa-check-circle"></i>
	          </td>
	       </xsl:otherwise>
	       </xsl:choose>
	       <td >
	           <xsl:value-of select="Status"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="TestCaseId"></xsl:value-of>
	       </td>
	       <td >
	           <xsl:value-of select="ScenarioName"></xsl:value-of>
	       </td>
         </tr>
        </xsl:for-each>
       </tbody>
     </table>
     </div>
    </div> 
    
   <div style="display:none;margin-top:120px;" class="summaryclass">
     <!--<div id='step-status-dashboard' style='width: 60%;min-width:60%;max-width:60%;height: 200px; margin-left:171px;margin-right:170px;margin-bottom:32px;'></div>-->	
    <div id='piechart{$i}'></div>
    <div id='barchart{$i}'></div>
    <div id='summarytablediv{$i}'>
     <table style=" width:100% ">
      <thead>
	    <tr class="border">
           <th> Total </th>
           <th> Passed </th>
           <th> Failed  </th>
           <th> PercentagePassed </th>
           <th> PercentageFailed </th>
         </tr>
	  </thead>
	  <tbody>
	   <xsl:for-each select="TestSummary">
	     <tr>
	       <td style="text-align:center;">
	           <xsl:value-of select="Total"></xsl:value-of>
	       </td >
	       <td style="text-align:center;">
	           <xsl:value-of select="Passed"></xsl:value-of>
	       </td >
	       <td style="text-align:center;">
	           <xsl:value-of select="Failed"></xsl:value-of>
	       </td >
	        <td style="text-align:center;">
	           <xsl:value-of select="PercentagePassed"></xsl:value-of>
	       </td>
	        <td style="text-align:center;">
	           <xsl:value-of select="PercentageFailed"></xsl:value-of>
	       </td>
         </tr>
        </xsl:for-each>
       </tbody>
     </table>
     </div>
    </div> 
    </xsl:for-each>
    <div class="footer">feedback:Automationuser@tui.co.uk</div>
  </body>
 </html>
</xsl:template>
</xsl:stylesheet>